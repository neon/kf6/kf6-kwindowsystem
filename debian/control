Source: kf6-kwindowsystem
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               kf6-extra-cmake-modules,
               libwayland-dev,
               libx11-dev,
               libxcb-icccm4-dev,
               libxcb-keysyms1-dev,
               libxcb-res0-dev,
               libxcb-xfixes0-dev,
               libxcb1-dev,
               libxfixes-dev,
               libxrender-dev,
               pkgconf,
               pkg-kde-tools-neon,
               plasma-wayland-protocols,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-tools-dev,
               qt6-wayland-dev,
               qt6-wayland-dev-tools,
               wayland-protocols
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/frameworks/kwindowsystem
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kwindowsystem
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kwindowsystem.git

Package: kf6-kwindowsystem
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: kwayland-integration (<< 4:5.27.10)
Replaces: libkf6windowsystem6,
          libkf6windowsystem-data,
          kwayland-integration (<< 4:5.27.10)
Description: Convenience access to certain properties and features of the window manager
 The class KWindowSystem provides information about the state of the
 window manager and allows asking the window manager to change them
 using a more high-level interface than the NETWinInfo/NETRootInfo
 low level classes.
 .
 This package contains the translations.

Package: kf6-kwindowsystem-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-kwindowsystem (= ${binary:Version}),
         qt6-base-dev,
         qt6-declarative-dev,
         qt6-tools-dev,
         qt6-wayland-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kf6-kwindowsystem (= ${source:Version})
Replaces: libkf6windowsystem-dev,
          libkf6windowsystem-doc,
Description: development files for kwindowsystem
 The class KWindowSystem provides information about the state of the
 window manager and allows asking the window manager to change them
 using a more high-level interface than the NETWinInfo/NETRootInfo
 lowlevel classes.
 .
 Contains development files for kwindowsystem.

Package: libkf6windowsystem6
Architecture: all
Depends: kf6-kwindowsystem, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6windowsystem-data
Architecture: all
Depends: kf6-kwindowsystem, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6windowsystem-dev
Architecture: all
Depends: kf6-kwindowsystem-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6windowsystem-doc
Architecture: all
Depends: kf6-kwindowsystem-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
